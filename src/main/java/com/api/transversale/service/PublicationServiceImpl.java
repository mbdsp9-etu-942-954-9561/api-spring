package com.api.transversale.service;

import com.api.transversale.modele.Commentaire;
import com.api.transversale.modele.Publication;
import com.api.transversale.repository.CommentaireRepository;
import com.api.transversale.repository.PublicationRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PublicationServiceImpl implements PublicationService{
	@Autowired
	private PublicationRepository publicationRepository;
	private CommentaireRepository commentaireRepository;

	@Override
	public Publication publier(Publication publication) {
		return publicationRepository.save(publication);
	}
	
	@Override
	public List<Publication> actualite(){
		return publicationRepository.findTop10ByOrderByPublicationDateDesc();
	}

	@Override
	public List<Publication> lire(){
		return publicationRepository.findAll();
	}

	@Override
	public Commentaire commenter(Integer idPublication, Commentaire commentaire) {
		Optional<Publication> publication = publicationRepository.findById(idPublication);
		if (publication.isPresent())
			commentaire.setPublication(publication.get());
		else
			throw new RuntimeException("Publication introuvable");

		return commentaireRepository.save(commentaire);
	}

	@Override
	public List<Commentaire> commentaire(Integer idPublication){
		return commentaireRepository.findByPublicationId(idPublication);
	}

}
