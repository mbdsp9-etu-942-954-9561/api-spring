package com.api.transversale.service;

import java.util.List;

import com.api.transversale.modele.Membrefamille;

public interface MembrefamilleService {
	Membrefamille creer(Membrefamille membrefamille);
	
	List<Membrefamille> lire();
	
	Membrefamille modifier(Integer id, Membrefamille membrefamille);
	
	String supprimer(Integer id);
	
	Membrefamille lireparId(Integer id);

	Membrefamille findByCitizenId(Integer citizenId);

	List<Membrefamille> livretFamille(Integer citizenId);
}
