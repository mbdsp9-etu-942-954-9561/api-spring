/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.service;

import com.api.transversale.modele.Investissement;
import com.api.transversale.modele.Projet;

import java.util.List;

/**
 *
 * @author Henintsoa
 */
public interface ProjetService {
    Projet creer(Projet famille);
	
    List<Projet> lire();
    
    Projet modifier(Integer id, Projet citoyen);
	
    String supprimer(Integer id);
    
    Projet lireparId(Integer id);

    Double avancement(Integer id);

    Double avancementBudget(Integer id);

    List<Investissement> listeInvestissement(Integer id, Integer type);
}
