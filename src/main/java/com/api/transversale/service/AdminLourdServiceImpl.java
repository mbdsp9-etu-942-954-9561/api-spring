package com.api.transversale.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.transversale.modele.AdminLourd;
import com.api.transversale.repository.AdminLourdRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AdminLourdServiceImpl implements AdminLourdService{
	@Autowired
	private AdminLourdRepository adminLourdRepository;
	
	@Override
	public AdminLourd creer(AdminLourd adminLourd) {
		return adminLourdRepository.save(adminLourd);
	}
	
	@Override
	public List<AdminLourd> lire(){
		return adminLourdRepository.findAll();
	}
	
	@Override
	public AdminLourd modifier(Integer id, AdminLourd adminLourd) {
		return adminLourdRepository.findById(id)
				.map(al->{
					al.setUsername(adminLourd.getUsername());
					al.setPassword(adminLourd.getPassword());
					return adminLourdRepository.save(al);
				}).orElseThrow(()-> new RuntimeException("AdminLourd non trouver"));
	}
	
	@Override
	public String supprimer(Integer id) {
		adminLourdRepository.deleteById(id);
		return "Citoyen supprimer";
	}
	
	@Override
	public AdminLourd lireparId(Integer id) {
		return adminLourdRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("AdminLourd non trouver par l'id: " + id));
	}
	
	@Override
    public AdminLourd findByUsername(String username) {
        return adminLourdRepository.findByUsername(username);
    }
}
