package com.api.transversale.service;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.utils.Dto;

import java.util.List;

public interface CitoyenService {
	Citoyen creer(Citoyen citoyen);
	
	List<Citoyen> lire();
	
	Citoyen modifier(Integer id, Citoyen citoyen);
	
	String supprimer(Integer id);
	
	Citoyen lireparId(Integer id);

    Citoyen logIn(Dto.SignInDto signInDto);
}
