package com.api.transversale.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.Membrefamille;
import com.api.transversale.repository.CitoyenRepository;
import com.api.transversale.repository.MembrefamilleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.api.transversale.modele.Famille;
import com.api.transversale.repository.FamilleRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FamilleServiceImpl implements FamilleService {
    @Autowired
    private FamilleRepository familleRepository;
    @Autowired
    private CitoyenRepository citoyenRepository;
    @Autowired
    private MembrefamilleRepository membrefamilleRepository;

    @Override
    public Famille creer(Famille famille) {
        Optional<Citoyen> citoyen = citoyenRepository.findById(famille.getFamilylead().getId());
        try {
            if ("Boy".equals(citoyen.get().getGender())) {
                Famille famille1 = familleRepository.save(famille);

                Membrefamille membrefamille = new Membrefamille(famille1, famille.getFamilylead(), new Date());
                membrefamilleRepository.save(membrefamille);
                return famille1;
            } else {
                throw new IllegalArgumentException("Le genre du family lead doit être 'Boy'");
            }
        } catch (Exception e) {
            throw new RuntimeException("Une erreur s'est produite lors de la création de la famille.", e);
        }
    }

    @Override
    public List<Famille> lire() {
        return familleRepository.findAll();
    }

    @Override
    public Famille lireparId(Integer id) {
        return familleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Famille non trouver par l'id: " + id));
    }

    @Override
    public Citoyen scanQrCode(String qrcode) {
        List<Famille> famille = familleRepository.findByQrcode(qrcode);
        if (famille.size() != 0)
            throw new RuntimeException("Famille non trouvé");

        return famille.get(0).getFamilylead();
    }

    @Override
    public List<Membrefamille> getMembreFamille(Integer id) {
        return membrefamilleRepository.findByFamilyId(id);
    }
}
