/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.service;

import com.api.transversale.modele.Investissement;

import java.util.List;

/**
 *
 * @author Henintsça
 */
public interface InvestissementService {
    Investissement creer(Investissement investissement);
	
    List<Investissement> lire();

    Investissement modifier(Integer id, Investissement investissement);
	
    String supprimer(Integer id);
    
    Investissement lireparId(Integer id);
}
