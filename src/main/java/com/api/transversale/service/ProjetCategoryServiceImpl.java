package com.api.transversale.service;

import com.api.transversale.modele.ProjetCategory;
import com.api.transversale.repository.ProjetCategoryRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class ProjetCategoryServiceImpl implements ProjetCategoryService{
	@Autowired
	private ProjetCategoryRepository projetCategoryRepository;
	
	@Override
	public ProjetCategory creer(ProjetCategory lifeStatus) {
		return projetCategoryRepository.save(lifeStatus);
	}
	
	@Override
	public List<ProjetCategory> lire(){
		return projetCategoryRepository.findAll();
	}
	



}
