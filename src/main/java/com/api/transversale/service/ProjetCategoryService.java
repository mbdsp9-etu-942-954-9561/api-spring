package com.api.transversale.service;

import com.api.transversale.modele.LifeStatus;
import com.api.transversale.modele.ProjetCategory;

import java.util.List;

public interface ProjetCategoryService {

	ProjetCategory creer(ProjetCategory lifeStatus);

	List<ProjetCategory> lire();

}
