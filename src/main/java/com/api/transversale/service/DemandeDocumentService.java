package com.api.transversale.service;

import java.util.List;

import com.api.transversale.modele.DemandeDocument;

public interface DemandeDocumentService {
	DemandeDocument creer(DemandeDocument demandeDocument);
	
	List<DemandeDocument> lire();
	
	DemandeDocument modifier(Integer id, DemandeDocument demandeDocument);
	
	String supprimer(Integer id);
	
	DemandeDocument lireparId(Integer id);
}
