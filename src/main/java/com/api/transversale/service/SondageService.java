/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.service;

import com.api.transversale.modele.Sondage;
import com.api.transversale.modele.utils.Dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Henintsoa
 */
public interface SondageService {
    Sondage creer(Sondage sondage);
	
    List<Sondage> lire();
    
    Sondage modifier(Integer id, Sondage sondage);
	
    String supprimer(Integer id);
    
    Sondage lireparId(Integer id);

    Boolean canVote(Integer citizenId, Integer projetId);

    ArrayList<Dto.ProjetVote> canVote(Integer citizenId);

    Dto.ProjetVote projetDetail(Integer citizenId, Integer projetId);

    Dto.ProjetPrediction prediction(Integer projetId);
}
