/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.service;

import com.api.transversale.modele.Projet;
import com.api.transversale.modele.Sondage;
import com.api.transversale.modele.utils.Dto;
import com.api.transversale.repository.ProjetRepository;
import com.api.transversale.repository.SondageRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Henintsoa
 */
@Service
@AllArgsConstructor
public class SondageServiceImpl implements SondageService {
    @Autowired
    private SondageRepository sondageRepository;

    @Autowired
    private ProjetRepository projetRepository;


    @Override
    public Sondage creer(Sondage sondage) {
        return sondageRepository.save(sondage);
    }

    @Override
    public List<Sondage> lire() {
        return sondageRepository.findAll();
    }

    @Override
    public Sondage modifier(Integer id, Sondage projet) {
        return sondageRepository.findById(id)
                .map(c -> {
                    c.setChoice(projet.getChoice());
                    return sondageRepository.save(c);
                }).orElseThrow(() -> new RuntimeException("Projet non trouver"));
    }

    @Override
    public String supprimer(Integer id) {
        sondageRepository.deleteById(id);
        return "Projet supprimer";
    }

    @Override
    public Sondage lireparId(Integer id) {
        return sondageRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Sondage non trouver par l'id: " + id));
    }

    @Override
    public Boolean canVote(Integer citizenId, Integer projetId) {
        List<Sondage> sondages = sondageRepository.findByCitizenIdAndProjetId(citizenId, projetId);
        if (sondages.size() != 1)
            return true;
        return false;
    }


    @Override
    public ArrayList<Dto.ProjetVote> canVote(Integer citizenId) {
        List<Projet> projets = projetRepository.findAll();
        ArrayList<Dto.ProjetVote> projetVotes = new ArrayList<>();
        for (Projet projet : projets) {
            boolean b = this.canVote(citizenId, projet.getId());
            long total = sondageRepository.countByProjetId(projet.getId());
            long forVote = sondageRepository.countByProjetIdAndChoice(projet.getId(), 1);
            projetVotes.add(new Dto.ProjetVote(projet, b, total, forVote));
        }
        return projetVotes;
    }


    @Override
    public Dto.ProjetVote projetDetail(Integer citizenId, Integer projetId) {
        Optional<Projet> optionalProjet = projetRepository.findById(projetId);
        if (!optionalProjet.isPresent())
            throw new RuntimeException("Projet non trouvé");
        Projet projet = optionalProjet.get();
        boolean b = this.canVote(citizenId, projet.getId());
        long total = sondageRepository.countByProjetId(projet.getId());
        long forVote = sondageRepository.countByProjetIdAndChoice(projet.getId(), 1);
        return new Dto.ProjetVote(projet, b, total, forVote);
    }

    @Override
    public Dto.ProjetPrediction prediction(Integer projetId) {
        Optional<Projet> optionalProjet = projetRepository.findById(projetId);
        if (!optionalProjet.isPresent())
            throw new RuntimeException("Projet non trouvé");
        Projet projet = optionalProjet.get();
        long yesVote = sondageRepository.countByProjetProjetCategoryIdAndChoice(projet.getProjetCategory().getId(), 1);
        long total = sondageRepository.countByProjetProjetCategoryId(projet.getProjetCategory().getId());

        double prediction = -1;
        if (total != 0) {
            prediction = (yesVote * 100) / total;
        }

        return new Dto.ProjetPrediction(projet, total, yesVote, prediction);
    }


}
