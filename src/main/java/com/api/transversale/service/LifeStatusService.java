package com.api.transversale.service;

import com.api.transversale.modele.LifeStatus;

import java.util.List;

public interface LifeStatusService {
	LifeStatus creer(LifeStatus lifeStatus);
	
	List<LifeStatus> lire();
	
	LifeStatus modifier(Integer id, LifeStatus lifeStatus);
	
	String supprimer(Integer id);
	
	LifeStatus lireparId(Integer id);
}
