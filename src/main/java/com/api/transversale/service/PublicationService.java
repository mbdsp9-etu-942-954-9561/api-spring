package com.api.transversale.service;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.Commentaire;
import com.api.transversale.modele.Publication;

import java.util.List;

public interface PublicationService {


	Publication publier(Publication publication);

	List<Publication> actualite();

	List<Publication> lire();

	Commentaire commenter(Integer idPublication, Commentaire commentaire);

	List<Commentaire> commentaire(Integer idPublication);
}
