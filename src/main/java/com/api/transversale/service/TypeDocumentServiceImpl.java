package com.api.transversale.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.transversale.modele.TypeDocument;
import com.api.transversale.repository.TypeDocumentRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TypeDocumentServiceImpl implements TypeDocumentService{
	@Autowired
	private TypeDocumentRepository typeDocumentRepository;
	
	@Override
	public TypeDocument creer(TypeDocument typeDocument) {
		return typeDocumentRepository.save(typeDocument);
	}
	
	@Override
	public List<TypeDocument> lire(){
		return typeDocumentRepository.findAll();
	}
	
	@Override
	public TypeDocument modifier(Integer id, TypeDocument typeDocument) {
		return typeDocumentRepository.findById(id)
				.map(td->{
					td.set_description(typeDocument.get_description());
					return typeDocumentRepository.save(td);
				}).orElseThrow(()-> new RuntimeException("Type de document non trouver"));
	}
	
	@Override
	public String supprimer(Integer id) {
		typeDocumentRepository.deleteById(id);
		return "Type document supprimer";
	}
	
	@Override
	public TypeDocument lireparId(Integer id) {
		return typeDocumentRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Type de document non trouver par l'id: " + id));
	}
}
