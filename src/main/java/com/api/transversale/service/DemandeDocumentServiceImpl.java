package com.api.transversale.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.transversale.modele.DemandeDocument;
import com.api.transversale.repository.DemandeDocumentRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class DemandeDocumentServiceImpl implements DemandeDocumentService{
	@Autowired
	private DemandeDocumentRepository demandeDocumentRepository;
	
	@Override
	public DemandeDocument creer(DemandeDocument demandeDocument) {
		return demandeDocumentRepository.save(demandeDocument);
	}
	
	@Override
	public List<DemandeDocument> lire(){
		return demandeDocumentRepository.findAll();
	}
	
	@Override
	public DemandeDocument modifier(Integer id, DemandeDocument demandeDocument) {
		return demandeDocumentRepository.findById(id)
				.map(dd->{
					dd.setCitizen(demandeDocument.getCitizen());
					dd.setDocument(demandeDocument.getDocument());
					dd.setRequestDate(demandeDocument.getRequestDate());
					return demandeDocumentRepository.save(dd);
				}).orElseThrow(()-> new RuntimeException("Demande de document non trouver"));
	}
	
	@Override
	public String supprimer(Integer id) {
		demandeDocumentRepository.deleteById(id);
		return "DemandeDocument supprimer";
	}
	
	@Override
	public DemandeDocument lireparId(Integer id) {
		return demandeDocumentRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Demande de document non trouver par l'id: " + id));
	}
}
