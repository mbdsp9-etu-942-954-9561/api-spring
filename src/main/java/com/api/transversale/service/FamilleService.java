package com.api.transversale.service;

import java.util.List;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.Famille;
import com.api.transversale.modele.Membrefamille;

public interface FamilleService {
	Famille creer(Famille famille);
	
	List<Famille> lire();
	
	Famille lireparId(Integer id);

	Citoyen scanQrCode(String qrcode);

    List<Membrefamille> getMembreFamille(Integer id);
}
