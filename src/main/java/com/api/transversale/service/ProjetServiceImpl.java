/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.service;

import com.api.transversale.modele.Investissement;
import com.api.transversale.modele.Projet;
import com.api.transversale.repository.InvestissementRepository;
import com.api.transversale.repository.ProjetRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Henintsoa
 */
@Service
@AllArgsConstructor
public class ProjetServiceImpl implements ProjetService{
    @Autowired
    private ProjetRepository projetRepository;
    @Autowired
    private InvestissementRepository investissementRepository;
    
    @Override
    public Projet creer(Projet projet) {
            return projetRepository.save(projet);
    }

    @Override
    public List<Projet> lire(){
            return projetRepository.findAll();
    }

    @Override
    public Projet modifier(Integer id, Projet projet) {
            return projetRepository.findById(id)
                            .map(c->{
                                    c.setTitle(projet.getTitle());
                                    c.set_description(projet.get_description());
                                    c.setBudget(projet.getBudget());
                                    c.setStartDate(projet.getStartDate());
                                    c.setDueDate(projet.getDueDate());
                                    return projetRepository.save(c);
                            }).orElseThrow(()-> new RuntimeException("Projet non trouver"));
    }

    @Override
    public String supprimer(Integer id) {
            projetRepository.deleteById(id);
            return "Projet supprimer";
    }

    @Override
    public Projet lireparId(Integer id) {
        return projetRepository.findById(id)
                            .orElseThrow(() -> new RuntimeException("Projet non trouver par l'id: " + id));
    }


    @Override
    public Double avancement(Integer id) {
        Projet projet =  projetRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Projet non trouver par l'id: " + id));
        Date currentDate = new Date();
        Date startDate = projet.getStartDate();
        Date dueDate = projet.getDueDate();

        long tempsEcoule = currentDate.getTime() - startDate.getTime();
        long dureeTotale = dueDate.getTime() - startDate.getTime();

        double pourcentageAvancement = (tempsEcoule / (double) dureeTotale) * 100.0;

        // Limiter le pourcentage entre 0 et 100
        pourcentageAvancement = Math.min(100.0, Math.max(0.0, pourcentageAvancement));
        return pourcentageAvancement;
    }

    @Override
    public Double avancementBudget(Integer id) {
        Projet projet =  projetRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Projet non trouver par l'id: " + id));
        List<Investissement> invests =  investissementRepository.findByProjetId(id);

        Double total = 0.0;
        for (Integer i = 0; i <= invests.size(); i++) {
            total += (invests.get(i).getPrice() * invests.get(i).getType());
        }

        double pourcentageAvancement = (total / (double) projet.getBudget()) * 100.0;
        pourcentageAvancement = Math.min(100.0, Math.max(0.0, pourcentageAvancement));
        return pourcentageAvancement;
    }
    @Override
    public List<Investissement> listeInvestissement(Integer id, Integer type){
        return investissementRepository.findByProjetIdAndType(id, type);
    }
}
