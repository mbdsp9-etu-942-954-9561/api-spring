package com.api.transversale.service;

import java.util.List;

import com.api.transversale.modele.TypeDocument;

public interface TypeDocumentService {
	TypeDocument creer(TypeDocument typeDocument);
	
	List<TypeDocument> lire();
	
	TypeDocument modifier(Integer id, TypeDocument typeDocument);
	
	String supprimer(Integer id);
	
	TypeDocument lireparId(Integer id);
}
