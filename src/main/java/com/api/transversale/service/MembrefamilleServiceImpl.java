package com.api.transversale.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.transversale.modele.Membrefamille;

import com.api.transversale.repository.MembrefamilleRepository;


import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class MembrefamilleServiceImpl implements MembrefamilleService {
    @Autowired
    private MembrefamilleRepository membrefamilleRepository;

    @Override
    public Membrefamille creer(Membrefamille membrefamille) {
        return membrefamilleRepository.save(membrefamille);
    }

    @Override
    public List<Membrefamille> lire() {
        return membrefamilleRepository.findAll();
    }

    @Override
    public Membrefamille modifier(Integer id, Membrefamille membrefamille) {
        return membrefamilleRepository.findById(id)
                .map(mf -> {
                    mf.setFamily(membrefamille.getFamily());
                    mf.setCitizen(membrefamille.getCitizen());
                    mf.setLastupdate(membrefamille.getLastupdate());
                    return membrefamilleRepository.save(mf);
                }).orElseThrow(() -> new RuntimeException("Membre de famille non trouver"));
    }

    @Override
    public String supprimer(Integer id) {
        membrefamilleRepository.deleteById(id);
        return "Membre de famille supprimer";
    }

    @Override
    public Membrefamille lireparId(Integer id) {
        return membrefamilleRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Membre famille non trouver par l'id: " + id));
    }

    @Override
    public Membrefamille findByCitizenId(Integer citizenId) {
        Membrefamille membrefamille = membrefamilleRepository.findByCitizenId(citizenId);
        if (membrefamille == null)
            throw new RuntimeException("Membre famille non trouver ");
        return membrefamille;
    }


    @Override
    public List<Membrefamille> livretFamille(Integer citizenId) {
        Membrefamille membrefamille = findByCitizenId(citizenId);
        return membrefamilleRepository.findByFamilyId(membrefamille.getFamily().getId());
    }

}
