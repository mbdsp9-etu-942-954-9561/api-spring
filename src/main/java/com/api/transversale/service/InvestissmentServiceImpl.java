/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.service;

import com.api.transversale.modele.Investissement;
import com.api.transversale.repository.InvestissementRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author Henintsoa
 */
@Service
@AllArgsConstructor
public class InvestissmentServiceImpl implements InvestissementService {
    @Autowired
    private InvestissementRepository investissementRepository;
    
    @Override
    public Investissement creer(Investissement investissement) {
            return investissementRepository.save(investissement);
    }

    @Override
    public List<Investissement> lire(){
            return investissementRepository.findAll();
    }



    @Override
    public Investissement modifier(Integer id, Investissement investissement) {
            return investissementRepository.findById(id)
                            .map(c->{
                                    c.setLabel(investissement.getLabel());
                                    c.setPrice(investissement.getPrice());
                                    c.setType(investissement.getType());
                                    return investissementRepository.save(c);
                            }).orElseThrow(()-> new RuntimeException("Investissement non trouver"));
    }

    @Override
    public String supprimer(Integer id) {
            investissementRepository.deleteById(id);
            return "investissement supprimer";
    }

    @Override
    public Investissement lireparId(Integer id) {
        return investissementRepository.findById(id)
                            .orElseThrow(() -> new RuntimeException("investissement non trouver par l'id: " + id));
    }
}
