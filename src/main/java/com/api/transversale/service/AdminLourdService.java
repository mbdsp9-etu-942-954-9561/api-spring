package com.api.transversale.service;

import java.util.List;

import com.api.transversale.modele.AdminLourd;

public interface AdminLourdService {
	AdminLourd creer(AdminLourd adminLourd);
	
	List<AdminLourd> lire();
	
	AdminLourd modifier(Integer id, AdminLourd citoyen);
	
	String supprimer(Integer id);
	
	AdminLourd lireparId(Integer id);
	
	AdminLourd findByUsername(String username);
}
