package com.api.transversale.service;

import java.util.List;

import com.api.transversale.modele.utils.Dto;
import com.api.transversale.repository.CitoyenRepository;
import com.api.transversale.modele.Citoyen;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CitoyenServiceImpl implements CitoyenService{
	@Autowired
	private CitoyenRepository citoyenRepository;
	
	@Override
	public Citoyen creer(Citoyen citoyen) {
		return citoyenRepository.save(citoyen);
	}
	
	@Override
	public List<Citoyen> lire(){
		return citoyenRepository.findAll();
	}
	
	@Override
	public Citoyen modifier(Integer id, Citoyen citoyen) {
		return citoyenRepository.findById(id)
				.map(c->{
					c.setName(citoyen.getName());
					c.setFirstname(citoyen.getFirstname());
					c.setBirthday(citoyen.getBirthday());
					c.setIdcard(citoyen.getIdcard());
					c.setDeathdate(citoyen.getDeathdate());
					c.setLifeStatus(citoyen.getLifeStatus());
					return citoyenRepository.save(c);
				}).orElseThrow(()-> new RuntimeException("Citoyen non trouver"));
	}
	
	@Override
	public String supprimer(Integer id) {
		citoyenRepository.deleteById(id);
		return "Citoyen supprimer";
	}
	
	@Override
	public Citoyen lireparId(Integer id) {
		return citoyenRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Citoyen non trouver par l'id: " + id));
	}

	@Override
	public Citoyen logIn(Dto.SignInDto signInDto) {
		List<Citoyen> citoyens = citoyenRepository.findByIdcardAndPassword(signInDto.getIdcard(), signInDto.getPassword());
		if (citoyens.size()!=1)
			throw new RuntimeException("Membre de Famille non trouver");

		return citoyens.get(0);
	}
}
