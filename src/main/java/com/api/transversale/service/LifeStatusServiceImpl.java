package com.api.transversale.service;

import com.api.transversale.modele.LifeStatus;
import com.api.transversale.repository.LifeStatusRepository;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@NoArgsConstructor
public class LifeStatusServiceImpl implements LifeStatusService{
	@Autowired
	private LifeStatusRepository lifeStatusRepository;
	
	@Override
	public LifeStatus creer(LifeStatus lifeStatus) {
		return lifeStatusRepository.save(lifeStatus);
	}
	
	@Override
	public List<LifeStatus> lire(){
		return lifeStatusRepository.findAll();
	}
	
	@Override
	public LifeStatus modifier(Integer id, LifeStatus lifeStatus) {
		return lifeStatusRepository.findById(id)
				.map(ls->{
					ls.setLabel(lifeStatus.getLabel());
					return lifeStatusRepository.save(ls);
				}).orElseThrow(()-> new RuntimeException("LifeStatus non trouver"));
	}
	
	@Override
	public String supprimer(Integer id) {
		lifeStatusRepository.deleteById(id);
		return "LifeStatus supprimer";
	}
	
	@Override
	public LifeStatus lireparId(Integer id) {
		return lifeStatusRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("LifeStatus non trouver par l'id: " + id));
	}


}
