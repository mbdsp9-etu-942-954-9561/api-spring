package com.api.transversale.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.transversale.modele.TypeDocument;

public interface TypeDocumentRepository extends JpaRepository<TypeDocument, Integer>{

}
