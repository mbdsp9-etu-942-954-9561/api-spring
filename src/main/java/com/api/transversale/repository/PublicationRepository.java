package com.api.transversale.repository;

import com.api.transversale.modele.Commentaire;
import com.api.transversale.modele.Publication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PublicationRepository extends JpaRepository<Publication, Integer>{

    // Get news publications
    List<Publication> findTop10ByOrderByPublicationDateDesc();

}
