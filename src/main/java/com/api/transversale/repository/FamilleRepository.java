package com.api.transversale.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.transversale.modele.Famille;

import java.util.List;

public interface FamilleRepository extends JpaRepository<Famille, Integer>{

    List<Famille> findByQrcode(String qrCode);
}
