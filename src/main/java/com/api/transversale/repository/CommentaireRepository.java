package com.api.transversale.repository;

import com.api.transversale.modele.Commentaire;
import com.api.transversale.modele.Famille;
import com.api.transversale.modele.Publication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentaireRepository extends JpaRepository<Commentaire, Integer>{
    List<Commentaire> findByPublicationId(Integer id);
}
