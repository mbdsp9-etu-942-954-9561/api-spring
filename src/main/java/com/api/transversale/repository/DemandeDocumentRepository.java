package com.api.transversale.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.api.transversale.modele.DemandeDocument;

public interface DemandeDocumentRepository extends JpaRepository<DemandeDocument, Integer>{

	
}
