/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.repository;

import com.api.transversale.modele.Projet;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Henintsoa
 */
public interface ProjetRepository extends JpaRepository<Projet, Integer>{
    
}
