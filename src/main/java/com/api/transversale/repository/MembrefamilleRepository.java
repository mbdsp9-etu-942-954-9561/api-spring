package com.api.transversale.repository;

import com.api.transversale.modele.Membrefamille;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MembrefamilleRepository extends JpaRepository<Membrefamille, Integer> {

    Membrefamille findByCitizenId(Integer id);


    List<Membrefamille> findByFamilyId(Integer id);
}
