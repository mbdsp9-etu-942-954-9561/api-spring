package com.api.transversale.repository;

import com.api.transversale.modele.ProjetCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjetCategoryRepository extends JpaRepository<ProjetCategory, Integer>{

}
