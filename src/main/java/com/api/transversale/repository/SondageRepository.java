/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.repository;

import com.api.transversale.modele.Sondage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *
 * @author Henintsoa
 */
public interface SondageRepository extends JpaRepository<Sondage, Integer>{
    List<Sondage> findByCitizenIdAndProjetId(Integer citizenId, Integer projetId);

    long countByProjetIdAndChoice(Integer projetId, Integer choice);
    long countByProjetProjetCategoryIdAndChoice(Integer projetCategoryId, Integer choice);

    long countByProjetId(Integer projetId);
    long countByProjetProjetCategoryId(Integer projetCategoryId);
}
