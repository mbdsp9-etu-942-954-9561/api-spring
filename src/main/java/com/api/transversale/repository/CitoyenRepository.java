package com.api.transversale.repository;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.Membrefamille;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CitoyenRepository extends JpaRepository<Citoyen, Integer>{

    List<Citoyen> findByIdcardAndPassword(String username, String password);


}
