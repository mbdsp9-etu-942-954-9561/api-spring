package com.api.transversale.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.api.transversale.modele.AdminLourd;

public interface AdminLourdRepository extends JpaRepository<AdminLourd, Integer>{
	AdminLourd findByUsername(String username);
}
