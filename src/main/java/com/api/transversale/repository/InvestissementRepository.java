/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.api.transversale.repository;

import com.api.transversale.modele.Investissement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

/**
 *
 * @author Henintsoa
 */
public interface InvestissementRepository extends JpaRepository<Investissement, Integer> {
    ArrayList<Investissement> findByProjetId(Integer id);
    
    ArrayList<Investissement> findByProjetIdAndType(Integer id, Integer type);
}
