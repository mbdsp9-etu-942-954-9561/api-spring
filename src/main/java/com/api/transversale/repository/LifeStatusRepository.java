package com.api.transversale.repository;

import com.api.transversale.modele.LifeStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LifeStatusRepository extends JpaRepository<LifeStatus, Integer>{

}
