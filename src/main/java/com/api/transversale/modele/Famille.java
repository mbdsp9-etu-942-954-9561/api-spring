package com.api.transversale.modele;

//import javax.persistence.*;
import jakarta.persistence.*;
import lombok.*;

@Entity
@Table(name= "famille")
@NoArgsConstructor
@Getter
@Setter
public class Famille {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
    @JoinColumn(name = "familylead", referencedColumnName = "id")
	private Citoyen familylead;
	
	@Column(length = 250)
	private String qrcode;

}
