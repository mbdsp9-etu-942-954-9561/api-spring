/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.modele;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "sondage")
@NoArgsConstructor
@Getter
@Setter
public class Sondage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "idprojet", referencedColumnName = "id")
    private Projet projet;
    
    @ManyToOne
    @JoinColumn(name = "idcitizen", referencedColumnName = "id")
    private Citoyen citizen;
    
    private Integer choice;

}
