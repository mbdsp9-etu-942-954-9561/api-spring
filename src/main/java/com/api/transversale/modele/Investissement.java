/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.modele;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Table(name= "investissement")
@NoArgsConstructor
@Getter
@Setter
public class Investissement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @ManyToOne
    @JoinColumn(name = "idprojet", referencedColumnName = "id")
    private Projet projet;
    
    
    @Column(length = 50)
    private String label;

    private Double price ;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date transactionDate;

    @Column(name = "_type")
    private Integer type;

    @PrePersist
    public void prePersist() {
        if (transactionDate == null) {
            transactionDate = new Date();
        }
    }
    
}
