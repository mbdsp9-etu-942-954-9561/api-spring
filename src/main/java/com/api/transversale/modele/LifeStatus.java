package com.api.transversale.modele;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name= "lifestatus")
@NoArgsConstructor
@Getter
@Setter
public class LifeStatus {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length = 15)
	private String label;

	public LifeStatus(String label) {
		this.label = label;
	}
}
