package com.api.transversale.modele;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name= "membrefamille")
@NoArgsConstructor
@Getter
@Setter
public class Membrefamille {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
    @JoinColumn(name = "idfamily", referencedColumnName = "id")
	private Famille family;
	
	@OneToOne
    @JoinColumn(name = "idcitizen", referencedColumnName = "id")
	private Citoyen citizen;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date lastupdate;

	public Membrefamille(Famille family, Citoyen citizen, Date lastupdate) {
		this.family = family;
		this.citizen = citizen;
		this.lastupdate = lastupdate;
	}

	@PrePersist
	public void prePersist() {
		if (lastupdate == null) {
			lastupdate = new Date();
		}
	}
}
