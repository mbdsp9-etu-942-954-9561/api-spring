package com.api.transversale.modele;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "commentaire")
@NoArgsConstructor
@Getter
@Setter
public class Commentaire {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "idpublication", referencedColumnName = "id")
    private Publication publication;

    @ManyToOne
    @JoinColumn(name = "idcitizen", referencedColumnName = "id")
    private Citoyen citizen;

    @Column(length = 250)
    private String comment;

}
