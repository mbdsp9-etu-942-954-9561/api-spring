package com.api.transversale.modele;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name= "projetCategory")
@NoArgsConstructor
@Getter
@Setter
public class ProjetCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 15)
    private String label;

    public ProjetCategory(String label) {
        this.label = label;
    }
}