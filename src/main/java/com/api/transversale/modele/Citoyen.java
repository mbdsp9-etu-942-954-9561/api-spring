package com.api.transversale.modele;

//import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Table(name = "citoyen")
@NoArgsConstructor
@Getter
@Setter
public class Citoyen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 50)
    private String name;

    @Column(length = 50)
    private String firstname;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "birthday")
    private Date birthday;

    @Column(length = 10)
    private String gender;

    @Column(length = 50)
    private String idcard;

    @Temporal(TemporalType.TIMESTAMP) // Ou TemporalType.DATE si vous n'avez pas besoin de l'heure
    @Column(name = "deathdate")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deathdate;

    @ManyToOne
    @JoinColumn(name = "lifeStatus", referencedColumnName = "id")
    private LifeStatus lifeStatus;

    @Column(length = 150)
    private String password;

    @PrePersist
    public void prePersist() {
        if (password == null) {
            password = "12345678";
        }
    }

}
