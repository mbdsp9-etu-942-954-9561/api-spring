package com.api.transversale.modele.utils;

import com.api.transversale.modele.Projet;
import com.api.transversale.modele.Publication;

public class Dto {
    public static class SignInDto {

        String idcard, password;

        public SignInDto(String idcard, String password) {
            this.idcard = idcard;
            this.password = password;
        }

        public String getIdcard() {
            return idcard;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class ProjetVote {
        Projet projet;
        boolean canVote;
        long totalVote;
        long forVote;

        public ProjetVote(Projet projet, boolean canVote, long totalVote, long forVote) {
            this.projet = projet;
            this.canVote = canVote;
            this.totalVote = totalVote;
            this.forVote = forVote;
        }

    }

    public static class ProjetPrediction {
        Projet projet;
        long totalVote;
        long forVote;
        double prediction;

        public ProjetPrediction(Projet projet, long totalVote, long forVote, double prediction) {
            this.projet = projet;
            this.totalVote = totalVote;
            this.forVote = forVote;
            this.prediction = prediction;
        }

    }
}
