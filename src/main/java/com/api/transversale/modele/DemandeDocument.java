package com.api.transversale.modele;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@Table(name = "demandeDocument")
@NoArgsConstructor
@Getter
@Setter
public class DemandeDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "idcitizen", referencedColumnName = "id")
    private Citoyen citizen;

    @ManyToOne
    @JoinColumn(name = "iddocument", referencedColumnName = "id")
    private TypeDocument document;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date requestDate;

    @PrePersist
    public void prePersist() {
        if (requestDate == null) {
            requestDate = new Date();
        }
    }

}
