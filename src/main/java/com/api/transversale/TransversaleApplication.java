package com.api.transversale;

import com.api.transversale.modele.LifeStatus;
import com.api.transversale.modele.ProjetCategory;
import com.api.transversale.modele.TypeDocument;
import com.api.transversale.repository.LifeStatusRepository;
import com.api.transversale.repository.ProjetCategoryRepository;
import com.api.transversale.repository.TypeDocumentRepository;
import com.api.transversale.service.LifeStatusService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class TransversaleApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransversaleApplication.class, args);
    }

    @Bean
    public CommandLineRunner saveLifeStatus(LifeStatusRepository lifeStatusRepository, TypeDocumentRepository typeDocumentRepository, ProjetCategoryRepository projetCategoryRepository) {
        return args -> {
            List<LifeStatus> lifeStatuses = lifeStatusRepository.findAll();
            if (lifeStatuses.size() == 0) {
                lifeStatusRepository.saveAll(new ArrayList<LifeStatus>() {{
                    add(new LifeStatus("Single"));
                    add(new LifeStatus("Married"));
                    add(new LifeStatus("Widow"));
                }});
            }

            List<TypeDocument> typeDocuments = typeDocumentRepository.findAll();
            if (typeDocuments.size() == 0) {
                typeDocumentRepository.saveAll(new ArrayList<TypeDocument>() {{
                    add(new TypeDocument("CIN"));
                    add(new TypeDocument("Livret de famille"));
                }});
            }

            List<ProjetCategory> projetCategories = projetCategoryRepository.findAll();
            if (projetCategories.size() == 0) {
                projetCategoryRepository.saveAll(new ArrayList<ProjetCategory>() {{
                    add(new ProjetCategory("Infrastructure"));
                    add(new ProjetCategory("Social"));
                }});
            }

        };
    }

}
