package com.api.transversale.controller;

import java.util.List;

import com.api.transversale.modele.utils.Dto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.service.CitoyenService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/citoyen")
@AllArgsConstructor
@CrossOrigin("*")
public class CitoyenController {
	@Autowired
	private CitoyenService citoyenService;

	//
	@PostMapping("/create")
	public Citoyen create(@RequestBody Citoyen citoyen) {
		return citoyenService.creer(citoyen);
	}

	//
	@GetMapping("/read")
	public List<Citoyen> read(){
		return citoyenService.lire();
	}

	//
	@PutMapping("/update/{id}")
	public Citoyen update(@PathVariable Integer id, @RequestBody Citoyen citoyen) {
		return citoyenService.modifier(id,citoyen);
	}

	//
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return citoyenService.supprimer(id);
	}

	//
	@GetMapping("/readById/{id}")
	public Citoyen readById(@PathVariable Integer id){
		return citoyenService.lireparId(id);
	}

	@PostMapping("/login")
	public Citoyen login(@RequestBody Dto.SignInDto signInDto) {
		// Recherchez l'utilisateur dans la base de données par le cardId
		return citoyenService.logIn(signInDto);

	}
}
