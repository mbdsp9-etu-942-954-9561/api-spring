package com.api.transversale.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.api.transversale.modele.Membrefamille;
import com.api.transversale.service.MembrefamilleService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/membrefamille")
@AllArgsConstructor
@CrossOrigin("*")
public class MembrefamilleController {
	@Autowired
	private MembrefamilleService membrefamilleService;

	//
	@PostMapping("/create")
	public Membrefamille create(@RequestBody Membrefamille membrefamille) {
		return membrefamilleService.creer(membrefamille);
	}

	//
	@GetMapping("/read")
	public List<Membrefamille> read(){
		return membrefamilleService.lire();
	}

	//
	@PutMapping("/update/{id}")
	public Membrefamille update(@PathVariable Integer id, @RequestBody Membrefamille membrefamille) {
		return membrefamilleService.modifier(id,membrefamille);
	}

	//
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return membrefamilleService.supprimer(id);
	}

	@GetMapping("/livret/{citizenId}")
	public List<Membrefamille> livretDeFamille(@PathVariable Integer citizenId){
		return membrefamilleService.livretFamille(citizenId);
	}



}
