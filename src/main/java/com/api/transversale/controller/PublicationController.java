package com.api.transversale.controller;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.Commentaire;
import com.api.transversale.modele.Publication;
import com.api.transversale.service.PublicationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/publication")
@AllArgsConstructor
@CrossOrigin("*")
public class PublicationController {
	//
	@Autowired
	private PublicationService publicationService;

	//
	@PostMapping("/create")
	public Publication create(@RequestBody Publication publication) {
		return publicationService.publier(publication);
	}

	//
	@GetMapping("/read")
	public List<Publication> read(){
		return publicationService.lire();
	}

	@GetMapping("/actualite")
	public List<Publication> actualite(){
		return publicationService.actualite();
	}

	//
	@PostMapping("/commenter/{idpublication}")
	public Commentaire commenter(@PathVariable Integer idpublication, @RequestBody Commentaire commentaire) {
		return publicationService.commenter(idpublication, commentaire);
	}

	//
	@GetMapping("/commentaire/{id}")
	public List<Commentaire> commentaire(@PathVariable Integer id) {
		return publicationService.commentaire(id);
	}
	
}
