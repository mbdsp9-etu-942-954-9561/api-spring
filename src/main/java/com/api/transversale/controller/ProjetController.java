/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.controller;

/**
 *
 * @author Njakasoa
 */

import com.api.transversale.modele.Investissement;
import com.api.transversale.modele.Projet;
import com.api.transversale.service.ProjetService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projet")
@AllArgsConstructor
@CrossOrigin("*")
public class ProjetController {
    @Autowired
    private ProjetService projetService;

    @PostMapping("/create")
    public Projet create(@RequestBody Projet projet) {
            return projetService.creer(projet);
    }

    @GetMapping("/read")
    public List<Projet> read(){
            return projetService.lire();
    }

    @PutMapping("/update/{id}")
    public Projet update(@PathVariable Integer id, @RequestBody Projet projet) {
            return projetService.modifier(id,projet);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
            return projetService.supprimer(id);
    }

    @GetMapping("/readById/{id}")
    public Projet readById(@PathVariable Integer id){
            return projetService.lireparId(id);
    }

    @GetMapping("/getAvancement/{id}")
    public Double getAvancement(@PathVariable Integer id){
        return projetService.avancement(id);
    }

    @GetMapping("/getAvancementBudget/{id}")
    public Double getAvancementBudget(@PathVariable Integer id){
        return projetService.avancementBudget(id);
    }

    @GetMapping("/invest/{projetId}/{type}")
    public List<Investissement> listeInvestissement(@PathVariable Integer projetId, @PathVariable Integer type){
        return projetService.listeInvestissement(projetId, type);
    }



}
