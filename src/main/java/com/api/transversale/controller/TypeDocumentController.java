package com.api.transversale.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.api.transversale.modele.TypeDocument;
import com.api.transversale.service.TypeDocumentService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/typeDocument")
@AllArgsConstructor
@CrossOrigin("*")
public class TypeDocumentController {
	@Autowired
	private TypeDocumentService typeDocumentService;

	//
	@PostMapping("/create")
	public TypeDocument create(@RequestBody TypeDocument typeDocument) {
		return typeDocumentService.creer(typeDocument);
	}

	//
	@GetMapping("/read")
	public List<TypeDocument> read(){
		return typeDocumentService.lire();
	}

	//
	@PutMapping("/update/{id}")
	public TypeDocument update(@PathVariable Integer id, @RequestBody TypeDocument typeDocument) {
		return typeDocumentService.modifier(id,typeDocument);
	}

	//
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return typeDocumentService.supprimer(id);
	}

	//
	@GetMapping("/readById/{id}")
	public TypeDocument readById(@PathVariable Integer id){
		return typeDocumentService.lireparId(id);
	}

}
