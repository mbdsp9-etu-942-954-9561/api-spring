package com.api.transversale.controller;

import com.api.transversale.modele.ProjetCategory;
import com.api.transversale.service.ProjetCategoryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projetCategory")
@AllArgsConstructor
@CrossOrigin("*")
public class ProjetCategoryController {
	@Autowired
	private ProjetCategoryService projetCategoryService;

	//
	@PostMapping("/create")
	public ProjetCategory create(@RequestBody ProjetCategory projetCategory) {
		return projetCategoryService.creer(projetCategory);
	}

	//
	@GetMapping("/read")
	public List<ProjetCategory> read(){
		return projetCategoryService.lire();
	}


}
