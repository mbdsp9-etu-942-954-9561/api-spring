package com.api.transversale.controller;

import com.api.transversale.modele.Citoyen;
import com.api.transversale.modele.Famille;
import com.api.transversale.modele.Membrefamille;
import com.api.transversale.service.FamilleService;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/famille")
@AllArgsConstructor
@CrossOrigin("*")
public class FamilleController {
	@Autowired
	private FamilleService familleService;

	//
	@PostMapping("/create")
	public Famille create(@RequestBody Famille famille) {
		return familleService.creer(famille);
	}

	//
	@GetMapping("/read")
	public List<Famille> read(){
		return familleService.lire();
	}

	//
	@GetMapping("/readById/{id}")
	public Famille readById(@PathVariable Integer id){
		return familleService.lireparId(id);
	}

	@GetMapping("/scan/{qrcode}")
	public Citoyen logIn(@PathVariable String qrcode){
		return familleService.scanQrCode(qrcode);
	}

	@GetMapping("/membrefamille/{id}")
	public List<Membrefamille> getMembreFamille(@PathVariable Integer id){return  familleService.getMembreFamille(id);}
}
