/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.controller;

import com.api.transversale.modele.Sondage;
import com.api.transversale.modele.utils.Dto;
import com.api.transversale.service.SondageService;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Njakasoa
 */
@RestController
@RequestMapping("/api/sondage")
@AllArgsConstructor
@CrossOrigin("*")
public class SondageController {
    @Autowired
    private SondageService sondageService;

    @PostMapping("/create")
    public Sondage create(@RequestBody Sondage sondage) {
        return sondageService.creer(sondage);
    }

    @GetMapping("/read")
    public List<Sondage> read() {
        return sondageService.lire();
    }

    @PutMapping("/update/{id}")
    public Sondage update(@PathVariable Integer id, @RequestBody Sondage sondage) {
        return sondageService.modifier(id, sondage);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable Integer id) {
        return sondageService.supprimer(id);
    }

    @GetMapping("/readById/{id}")
    public Sondage readById(@PathVariable Integer id) {
        return sondageService.lireparId(id);
    }

    @GetMapping("/canvote/{citizenId}/{projetId}")
    public Boolean canVote(@PathVariable Integer citizenId, @PathVariable Integer projetId) {
        return sondageService.canVote(citizenId, projetId);
    }

    @GetMapping("/canvote/{citizenId}")
    public String canVote(@PathVariable Integer citizenId) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return om.writeValueAsString(sondageService.canVote(citizenId));
    }

    @GetMapping("/projetVote/{citizenId}/{projetId}")
    public String getProjetDetail(@PathVariable Integer citizenId, @PathVariable Integer projetId) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return om.writeValueAsString(sondageService.projetDetail(citizenId, projetId));
//        return sondageService.canVote(citizenId,projetId);
    }

    @GetMapping("/prediction/{projetId}")
    public String prediction(@PathVariable Integer projetId) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        return om.writeValueAsString(sondageService.prediction(projetId));
    }
}
