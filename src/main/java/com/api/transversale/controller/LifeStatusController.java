package com.api.transversale.controller;

import com.api.transversale.modele.LifeStatus;
import com.api.transversale.service.LifeStatusService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/lifestatus")
@AllArgsConstructor
@CrossOrigin("*")
public class LifeStatusController {
	@Autowired
	private LifeStatusService lifeStatusService;

	//
	@PostMapping("/create")
	public LifeStatus create(@RequestBody LifeStatus lifeStatus) {
		return lifeStatusService.creer(lifeStatus);
	}

	//
	@GetMapping("/read")
	public List<LifeStatus> read(){
		return lifeStatusService.lire();
	}

	//
	@PutMapping("/update/{id}")
	public LifeStatus update(@PathVariable Integer id, @RequestBody LifeStatus lifeStatus) {
		return lifeStatusService.modifier(id,lifeStatus);
	}

	//
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return lifeStatusService.supprimer(id);
	}

	//
	@GetMapping("/readById/{id}")
	public LifeStatus readById(@PathVariable Integer id){
		return lifeStatusService.lireparId(id);
	}

}
