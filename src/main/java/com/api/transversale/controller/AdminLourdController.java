package com.api.transversale.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.api.transversale.modele.AdminLourd;
import com.api.transversale.service.AdminLourdService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/adminLourd")
@AllArgsConstructor
@CrossOrigin("*")
public class AdminLourdController {
	@Autowired
	private AdminLourdService adminLourdService;

	//
	@PostMapping("/create")
	public AdminLourd create(@RequestBody AdminLourd adminLourd) {
		return adminLourdService.creer(adminLourd);
	}

	//
	@GetMapping("/read")
	public List<AdminLourd> read(){
		return adminLourdService.lire();
	}

	//
	@PutMapping("/update/{id}")
	public AdminLourd update(@PathVariable Integer id, @RequestBody AdminLourd adminLourd) {
		return adminLourdService.modifier(id,adminLourd);
	}

	//
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return adminLourdService.supprimer(id);
	}

	//
	@GetMapping("/readById/{id}")
	public AdminLourd readById(@PathVariable Integer id){
		return adminLourdService.lireparId(id);
	}

	//
	@PostMapping("/login")
    public AdminLourd login(@RequestBody AdminLourd loginRequest) {
        // Recherchez l'utilisateur dans la base de données par le nom d'utilisateur
        AdminLourd adminLourd = adminLourdService.findByUsername(loginRequest.getUsername());

        if (adminLourd.getPassword().equals(loginRequest.getPassword())) {
            return adminLourd;
        }
		throw new RuntimeException("Admin non trouver");
    }
	
	
}
