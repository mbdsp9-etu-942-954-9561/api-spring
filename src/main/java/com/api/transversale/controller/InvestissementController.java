/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.api.transversale.controller;

import com.api.transversale.modele.Investissement;
import com.api.transversale.service.InvestissementService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author Henintsoa
 */
@RestController
@RequestMapping("/api/investissement")
@AllArgsConstructor
@CrossOrigin("*")
public class InvestissementController {
    @Autowired
	private InvestissementService investissementService;
	
	@PostMapping("/create")
	public Investissement create(@RequestBody Investissement investissement) {
		return investissementService.creer(investissement);
	}
	
	@GetMapping("/read")
	public List<Investissement> read(){
		return investissementService.lire();
	}

	@GetMapping("/type/{type}")
	public List<Investissement> getByType(@PathVariable Integer type){
		return investissementService.lire();
	}
	
	@PutMapping("/update/{id}")
	public Investissement update(@PathVariable Integer id, @RequestBody Investissement investissement) {
		return investissementService.modifier(id,investissement);
	}
	
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return investissementService.supprimer(id);
	}
	
	@GetMapping("/readById/{id}")
	public Investissement readById(@PathVariable Integer id){
		return investissementService.lireparId(id);
	}
}
