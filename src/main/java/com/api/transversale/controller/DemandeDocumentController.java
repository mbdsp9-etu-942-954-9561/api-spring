package com.api.transversale.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.api.transversale.modele.DemandeDocument;
import com.api.transversale.service.DemandeDocumentService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api/demandeDocument")
@AllArgsConstructor
@CrossOrigin("*")
public class DemandeDocumentController {
	@Autowired
	private DemandeDocumentService demandeDocumentService;

	//
	@PostMapping("/create")
	public DemandeDocument create(@RequestBody DemandeDocument demandeDocument) {
		return demandeDocumentService.creer(demandeDocument);
	}

	//
	@GetMapping("/read")
	public List<DemandeDocument> read(){
		return demandeDocumentService.lire();
	}

	//
	@PutMapping("/update/{id}")
	public DemandeDocument update(@PathVariable Integer id, @RequestBody DemandeDocument demandeDocument) {
		return demandeDocumentService.modifier(id,demandeDocument);
	}

	//
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable Integer id) {
		return demandeDocumentService.supprimer(id);
	}

	//
	@GetMapping("/readById/{id}")
	public DemandeDocument readById(@PathVariable Integer id){
		return demandeDocumentService.lireparId(id);
	}
}
