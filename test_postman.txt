// RoleLourd
{
	"label": "Admin"
}

// AdminLourd
{
	"username": "username",
	"password": "password"
}
	
// Citoyen
{
	"name":"Jean",
	"firstname":"Koto",	
	"birthday":"01/01/2001",
	"idcard":"123456789",
	"ismarried": true,
	"lifeStatus": 0
}

//typeDocument
{
	 "name":"Lorem ipsum"
}

//DemandeDocument
{
	"citizen": {
		"id": 2
	},
	"document": {
		"id": 2
	}
}

// Family
{
	"familylead":{
		"id": 2
	},
	"qrcode": "123557874221" 
}	

// Membre Famille
{
	"family":{
		"id": 2
	},
	"citizen":{
		"id": 2
	}
}

// Publication
{
	"label": "label",
    "author": "author"
}

// Commentaire
{
    "publication":{
		"id": 1
	},
    "citizen":{
		"id": 2
	},
    "comment": "comment qwerty"
}